﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineTest : MonoBehaviour
{
    private Coroutine moveUp;
    private int frameCounter;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        frameCounter += 1;
        if (Input.GetButtonDown("Jump"))
        {
            moveUp = StartCoroutine(MoveUp());
        }
    }

    public IEnumerator MoveUp()
    {
        Debug.Log(frameCounter.ToString());
        yield return new WaitForSeconds(2f);
        Debug.Log(frameCounter.ToString());
    }
}
