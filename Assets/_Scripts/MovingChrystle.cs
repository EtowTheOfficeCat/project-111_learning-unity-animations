﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MovingChrystle : MonoBehaviour
{
    public float Duration = 1f;
    public Vector3 Offset;
    public Transform crystal;


    private void OnMouseDown()
    {
        crystal.DOMove(transform.position + Offset, 1f);
    }
}
