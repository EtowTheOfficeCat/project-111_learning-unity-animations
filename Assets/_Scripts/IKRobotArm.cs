﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKRobotArm : MonoBehaviour
{
    public GameObject MovableObject;
    public Transform[] SpawnPoints;
    public Color[] colors;
    public Transform Hand;
    public string[] ParameterNames;
    public Transform PathStart;
    public Transform PathEnd;
    
    private Animator animator;
    private GameObject curObj;

    private void Start()
    {
        animator = GetComponent<Animator>();
        Spawn();
    }
    public void Spawn()
    {
        int randIdx = Random.Range(0, SpawnPoints.Length);
        Vector3 spawnPos = SpawnPoints[randIdx].position;
        curObj = Instantiate(MovableObject, spawnPos, Quaternion.identity);
        curObj.GetComponentInChildren<Renderer>().material.color = colors[randIdx];
        animator.SetTrigger(ParameterNames[randIdx]);

        PathStart.transform.position = spawnPos;
        
    }

    public void GrabObject()
    {
        curObj.transform.parent = Hand;
        var nodes = iTweenPath.paths["MyWay"].nodes;

    }

    public void ReleaseObject()
    {
        curObj.GetComponent<Rigidbody>().isKinematic = false;
        curObj.transform.parent = null;

    }
}
