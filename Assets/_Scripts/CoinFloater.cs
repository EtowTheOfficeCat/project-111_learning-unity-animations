﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinFloater : MonoBehaviour
{
    [Range(0f,1f)]
    public float coinFloatingStrengh;
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetLayerWeight(1, coinFloatingStrengh);
    }
}
