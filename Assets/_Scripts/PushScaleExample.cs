﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushScaleExample : MonoBehaviour
{
    private Vector3 curScale;
    // Start is called before the first frame update
    void Start()
    {
        curScale = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown)
        {
            iTween.StopByName("restarter");
            transform.localScale = curScale;
        }
    }
}
