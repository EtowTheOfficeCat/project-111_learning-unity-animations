﻿using UnityEngine;

public class RobotArm : MonoBehaviour
{
    public GameObject MovableObject;
    public Transform[] SpawnPoints;
    public Color[] colors;
    public Transform Hand;
    public string[] ParameterNames;
    private Animator animator;
    private GameObject curObj;

    private void Start()
    {
        animator = GetComponent<Animator>();
        Spawn();
    }
    public void Spawn()
    {
        int randIdx = Random.Range(0, SpawnPoints.Length);
        Vector3 spawnPos = SpawnPoints[randIdx].position;
        curObj =  Instantiate(MovableObject, spawnPos, Quaternion.identity );
        curObj.GetComponentInChildren<Renderer>().material.color = colors[randIdx];
        animator.SetTrigger(ParameterNames[randIdx]);
    }

    public void GrabObject()
    {
        curObj.transform.parent = Hand;
        
    }

    public void ReleaseObject()
    {
        curObj.GetComponent<Rigidbody>().isKinematic = false;
        curObj.transform.parent = null;
        
    }

}
