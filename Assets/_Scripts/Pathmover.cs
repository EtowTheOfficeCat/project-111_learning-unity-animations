﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathmover : MonoBehaviour
{
    public float duration = 4f;
    private float timer;
    private bool isMovingfwd = true;
    

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (isMovingfwd)
        {
            iTween.PutOnPath(gameObject, iTweenPath.GetPath("MyWay"), timer / duration);
        }
        
        else
        {
            iTween.PutOnPath(gameObject, iTweenPath.GetPathReversed("MyWay"), timer / duration);
        }

        if (timer > duration)
        {
            isMovingfwd = !isMovingfwd;
            timer = 0f;
        }
    }
}
