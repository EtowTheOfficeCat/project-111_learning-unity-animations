﻿using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class ChrystalMover : MonoBehaviour, IPointerClickHandler
{
    public Transform crystal;
    public float Duration = 1f;
    public AnimationCurve MyCurve;
    public AnimationCurve MyPingPongCurve;
    private Vector3 lastPos;
    bool isTweening;
    public Color ActiveColor;


    private void Start()
    {
        lastPos = crystal.position;
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        GameObject curGo = eventData.pointerCurrentRaycast.gameObject;
        Renderer rend = curGo.GetComponentInChildren<Renderer>();
        Color originColor = rend.material.color;
        Sequence seq = DOTween.Sequence();
        seq.Append(rend.material.DOColor(ActiveColor, 0.3f).SetEase(MyPingPongCurve));
        seq.Append(rend.material.DOColor(originColor, 0.3f).SetEase(MyPingPongCurve));
        seq.Play();
        if (isTweening) { return; }
        isTweening = true;
        DOTween.KillAll();
        Vector3 pos = curGo.transform.parent.position;
        float distance = (pos - lastPos).magnitude;
        crystal.DOMove(pos, Duration * distance).
            SetEase(MyCurve).
            OnComplete(() => isTweening = false);
        lastPos = pos;
    }

    
}
