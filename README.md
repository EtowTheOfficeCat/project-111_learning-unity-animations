 # Unity Animations and more.
 
 Improving Knowledge of Unity animations as well as tools like Itween and DoTween, custom shaders and IK animations (unity Animation Rigging) 
 
 creating small test Animations like these :
 
 ![](https://media.giphy.com/media/gdqRjpgQWKA5anfKP7/giphy.gif)
 
 or creating Unity IK Rigging :
 
  ![](https://media.giphy.com/media/Q8gth4LpWPp06M0Xdw/giphy.gif)